checkTypeNumber = givenNumber => {
    if(typeof givenNumber === "number") {
        if (givenNumber % 2 == 0){
            return "genap"
        } else {
            return "ganjil"
        }
    }
    else if ( givenNumber == null) {
        return "error : Bro where is the parameter?"
    }else
        return "error : invalid data type";
    }

console.log(checkTypeNumber(10));
console.log(checkTypeNumber(3));
console.log(checkTypeNumber("3"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());