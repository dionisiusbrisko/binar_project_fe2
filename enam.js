function getAngkaTerbesarKedua(dataNumbers) {
  // validasi apakah parameter yang diterima berupa array
  if (!Array.isArray(dataNumbers)) {
    return "Parameter harus berupa array!";
  }

  // validasi apakah array yang diberikan memiliki minimal dua angka
  if (dataNumbers.length < 2) {
    return "Array harus memiliki minimal dua angka!";
  }

  // urutkan array dari yang terkecil ke terbesar
  dataNumbers.sort(function(a, b){return a - b});

  // ambil angka terbesar kedua dari array yang telah diurutkan
  var angkaTerbesarKedua = dataNumbers[dataNumbers.length - 2];

  // return angka terbesar kedua
  return angkaTerbesarKedua;
}

// contoh penggunaan function getAngkaTerbesarKedua
const dataAngka = [9,4,7,7,4,3,2,2,8]; 
// const nomor = [9,8,7,6,5,4,3,2,2,1]
console.log(getAngkaTerbesarKedua(dataAngka)); // output: 8
// console.log(getAngkaTerbesarKedua(nomor));
console.log(getAngkaTerbesarKedua("diooo")); // output: Parameter harus berupa array!
console.log(getAngkaTerbesarKedua(0)); // output: Parameter harus berupa array!
console.log(getAngkaTerbesarKedua());