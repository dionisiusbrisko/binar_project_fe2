function splitName(personName) {

    if (typeof personName !== "string") {
      return "Error: Invalid input type, expected string";
    }
  

    const nameParts = personName.trim().split(/\s+/);
  
    switch (nameParts.length) {
      case 1:
        return {
          firstName: nameParts[0],
          middleName: null,
          lastName: null,
        };
      case 2:
        return {
          firstName: nameParts[0],
          middleName: null,
          lastName: nameParts[1],
        };
      case 3:
        return {
          firstName: nameParts[0],
          middleName: nameParts[1],
          lastName: nameParts[2],
        };
      default:
        return "Error: Full name should consist of at most 3 words";
    }
  }
  
  console.log(splitName("Adi Daniela Pranata"));
  console.log(splitName("Dwi Kuncoro"));
  console.log(splitName("Aurora"));
  console.log(splitName(0));
  console.log(splitName(""));