
// nomor 1
function changeWorld(selectedText, changedText, text) {
    const a = text.replace(selectedText, changedText);
    // .replace adalah salah satu metod untuk string, parameter.replace(searchValue, replacenilai)
    return a;
  }
  
  const kalimat1 = "andini sangat mencintai kamu selamanya";
  const kalimat2 = "gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu"

  console.log(changeWorld("mencintai","membenci",kalimat1));
  console.log(changeWorld('bromo','semeru',kalimat2));
