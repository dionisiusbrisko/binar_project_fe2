
function checkEmail(email) {
    if (email === undefined) {
      return "Error: There is no email to check";
    }
    if (typeof email !== "string") {
      return "Error: Email should be a string";
    }
    if (!/@/.test(email)) {
      return "Error: Email should contain '@' character";
    }
    if (^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$.test(email)) {
      return "VALID"; 
    } else {
      return "INVALID";
    }
  }
  
  console.log(checkEmail("apranata@gmail.co.id"));
  console.log(checkEmail("apranata@gmail.com"));
  console.log(checkEmail("apranata@binar"));
  console.log(checkEmail("apranata"));
  console.log(checkEmail(3322))
  console.log(checkEmail());